# Ansible Collection - redhatautomation.aws

## Installation and Usage

### Installing the Collection

Before using the site_config collection, you need to install it.
You can also include it in a `requirements.yml` file and install it via `ansible-galaxy collection install -r requirements.yml`, using the format:

```yaml
---
collections:
  - name: https://gitlab.com/redhatautomation/aws.git
    type: git
```

```yaml
# response_url required for slack confirmation
vars:
  response_url: x.y.z
```

### Using the collection

```yaml
---
- name: Infrastructure Provisioning
  hosts: localhost
  gather_facts: no

  tasks:
    - name: Create AWS Infra
      ansible.builtin.include_role:
        name: redhatautomation.aws.aws_infrastructure

- name: RHEL Configuration
  hosts: AWS-WebServer
  gather_facts: no

  tasks:
    - name: Configure RHEL Server
      ansible.builtin.include_role:
        name: redhatautomation.aws.conf_webserver

    - name: Deploy Apache
      ansible.builtin.include_role:
        name: redhatautomation.aws.conf_apache_firewall

- name: Slack Confirmation
  hosts: localhost
  gather_facts: no

  tasks:
    - name: Slack Confirmation
      ansible.builtin.include_role:
        name: redhatautomation.aws.aws_slack_confirm
```
