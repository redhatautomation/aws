### What this role does

This role uses a slackbot integration to confirm setup and teardown of aws infrastructure. You will need to supply the slackurl.

# response_url required for slack confirmation
vars:
  response_url: x.y.z

### Using the role (example)

```yaml
---
- name: AWS Slack Confirmation
  hosts: localhost
  connection: local
  gather_facts: false

  roles:
    - role: redhatautomation.aws.aws_slack_confirm
```
