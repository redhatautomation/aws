### What this role does

conf_webserver role sources the files in files/apache in order to configure the IIS webserver in a default state

### Using the role (example)

```yaml
---
- name: Configure Webserver
  hosts: all
  become: yes
  gather_facts: false

  roles:
    - role: redhatautomation.aws.conf_webserver
```
